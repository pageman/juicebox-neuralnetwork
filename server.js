var brain = require('brain');
var request = require('request');
var _ = require('underscore');
var net = new brain.NeuralNetwork();

var Hapi = require('hapi');

var server = new Hapi.Server();
var port = Number(process.env.PORT || 4444);

server.connection({ port: port, routes: { cors: true } });

var log = net.train([
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":0, "obese":1, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeC" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":1, "normal":0, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeD" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeA" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 1, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 1, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeB" : 1 } },
  {"input": { "underWeight":0, "normal":1, "overWeight":0, "obese":0, "diabetes": 0, "hypertension": 0, "diarrhea": 1 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":0, "obese":1, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":0, "obese":1, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":0, "obese":1, "diabetes": 1, "hypertension": 0, "diarrhea": 0 }, "output": { "recipeF" : 1 } },
  {"input": { "underWeight":0, "normal":0, "overWeight":1, "obese":0, "diabetes": 0, "hypertension": 1, "diarrhea": 0 }, "output": { "recipeF" : 1 } }
]);

server.route([
  {
    method: 'POST',
    path: '/recipe/suggest',
    handler: function (request, reply) {
      console.log(request.payload);
      var obj = request.payload.data;

      var BMI = obj.result.weight / ((obj.result.height/100)^2);

      var underWeight = 0;
      var normal = 0;
      var overWeight = 0;
      var obese = 0;

      var diabetesStatus = 0;
      var hypertensionStatus = 0;
      var diarrheaStatus = 0;

      if(BMI < 18.5){
        underWeight = 1;
      } else if(BMI > 18.5 && BMI < 24.9){
        normal = 1;
      } else if(BMI > 25 && BMI < 29.9){
        overWeight = 1;
      } else if(BMI > 30 && BMI < 34.9){
        obese = 1;
      }

      var diabetes = _.find(obj.result.conditions, function(obj) { return obj.name == 'Diabetese' });
      var hypertension = _.find(obj.result.conditions, function(obj) { return obj.name == 'Hypertension' });
      var diarrhea = _.find(obj.result.conditions, function(obj) { return obj.name == 'Diarrhea' });

      if(diabetes !== undefined) {
        diabetesStatus = 1;
      }
      if(hypertension !== undefined){
        hypertensionStatus = 1
      }
      if(diarrhea !== undefined){
        diarrheaStatus = 1;
      }

      var dataToTrain = { "underWeight":underWeight, "normal":normal, "overWeight":overWeight, "obese":obese, "diabetes": diabetesStatus, "hypertension": hypertensionStatus, "diarrhea": diarrheaStatus };

      var output = net.run( dataToTrain );
      console.log(dataToTrain);
      console.log(output);
      var result = compareResults(output);
      console.log(result);
      reply({id:1, recommendedRecipe: result.recipeName, value: result.value});
    }
  }
]);

function compareResults( obj ){
  var maxProp = null
  var maxValue = -1
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      var value = obj[prop]
      if (value > maxValue) {
        maxProp = prop
        maxValue = value
      }
    }
  }
  var result = {
    recipeName: maxProp,
    value: maxValue
  }
  return result;
}

server.start();
